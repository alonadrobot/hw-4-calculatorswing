package com.Calculator;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
public class Calculate {


    public static void main(String args[]) {
        Calculator calculator=new Calculator();

        JFrame mainFrame = new JFrame("calculator");
        JLabel jLabelOne = new JLabel( "Число   1:");
        JLabel jLabelTwo = new JLabel( "Число   2:");
        JLabel jLabelTree = new JLabel("Оператор:");
        JLabel jLabelFor = new JLabel("Результат:");

        JTextField jTextFieldFirstNumber = new JTextField(20);
        JTextField jTextFieldSecondNumber= new JTextField(20);
        JTextField jTextFieldOperator= new JTextField(20);
        JTextField jTextFieldResult= new JTextField(20);

        JButton jButtonResult = new JButton();


        jButtonResult.setPreferredSize(new Dimension(250,50));
        jButtonResult.addActionListener(e -> {
            String operator;
            Integer firstNumber;
            Integer secondNumber;
            try {
                operator = jTextFieldOperator.getText();
                firstNumber = Integer.valueOf(jTextFieldFirstNumber.getText());
                secondNumber = Integer.valueOf(jTextFieldSecondNumber.getText());
                String result=calculator.calculator(firstNumber, secondNumber,operator);
                jTextFieldResult.setText(result);
            }catch (Exception ex){
                jTextFieldResult.setText("Неправильний ввід");
                return;
            }
        });

        JPanel jPanel = new JPanel();
        jPanel.add(jLabelOne);
        jPanel.add(jTextFieldFirstNumber);
        jPanel.add(jLabelTwo);
        jPanel.add(jTextFieldSecondNumber);
        jPanel.add(jLabelTree);
        jPanel.add(jTextFieldOperator);
        jPanel.add(jButtonResult);
        jPanel.add(jLabelFor);
        jPanel.add(jTextFieldResult);
        jPanel.setBackground(Color.getHSBColor(300,400,300));

        mainFrame.add(jPanel);
        mainFrame.setSize(300, 300);
        mainFrame.show();
    }
}
