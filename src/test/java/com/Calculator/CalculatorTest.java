package com.Calculator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    static Calculator cut = new Calculator();
    static Arguments[] calculatorTest(){
        return new Arguments[]{
                Arguments.arguments(5,5,"*","25"),
                Arguments.arguments(3,10,"+","13"),
                Arguments.arguments(20,5,"-","15"),
                Arguments.arguments(60,6,"/","10"),
        };
    }




    @ParameterizedTest
    @MethodSource("calculatorTest")
    void calculatorTest(int firstNumber, int secondNumber, String operator, String expected){
        String actual = cut.calculator(firstNumber, secondNumber, operator);
        Assertions.assertEquals(expected,actual);




    }


}